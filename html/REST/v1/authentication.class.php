<?PHP
// Handles initial authentication using HTTP Basic Auth
include_once $config['libspath'] . '/password.php'; // Needed because of old PHP version

class authentication {

	public function __construct() { }

	public function authenticate() {
		// Authenticates with basic auth and if successful returns jwt
		global $jwt, $db;

		$authParams = $jwt->getBasicAuthDetails();

        if ($authParams['user'] == null || $authParams['user'] == '') {
            $this->ErrorInvalidUserPass();
        }
        $email = strtolower($authParams['user']); // NOTE: strtolower

        $sql = $db->prepare("SELECT password, id, role FROM users WHERE `email` = ?");
        $sql->execute( [ $email ] );
        if (($row = $sql->fetch())) {
            if ($row->password == null) { $this->ErrorInvalidUserPass(); }
            if (password_verify($authParams['pass'], $row->password)) {
                // Authentication ok, generate jwt and update lastLoggedin
                $upd = $db->prepare("UPDATE users SET lastlogin_date = NOW() WHERE id = ?");
                $upd->execute( [$row->id] );

                $token = $jwt->makeToken([
                    'userid' => $row->id,
                    'email'  => $email,
                    'role'   => $row->role
                ]);

                success( ['jwt' => $token] );

            } else {
                error_log("Incorrect login for {$email}");
                $this->ErrorInvalidUserPass();
            }

        } else {
            error_log("Incorrect login for {$email}");
            $this->ErrorInvalidUserPass();
        }
	}

    public function renew() {
        // If used in frontends, the token should be renewed every few minutes in the background as the token doesn't auto-renew
        global $db, $jwt;
        // Check if we still have a valid token or exit
        $sess = $jwt->authenticate();

        // Update user login time
        $upd = $db->prepare("UPDATE users SET lastlogin_date = NOW() WHERE id = ?");
        $upd->execute( [$sess->userid] );

        // Make new token
        $token = $jwt->makeToken($sess);

        // Return token and have JS update the token (some overlap is ok, multiple tokens can exist in parallel)
        success( ['jwt' => $token] );
    }

    public function confirmToken($data) {
        // Used to check if user is still an existing user with the same role
        // if not, we can return the correct role OR return an unauthorized trigger
        // return possibilities:
        // return true - all is ok
        // return false - unauthorized
        // return $data - will overwrite data with fixed data package
        global $db;
        $sql = $db->prepare("SELECT email, role FROM users WHERE email = ?");
        $sql->execute([$data->email]);
        if (($user = $sql->fetch())) {
            if ($user->email != $data->email) { return false; } // Just to be sure
            if ($user->role != $data->role) {
                $data->role = $user->role;
                return $data;
            }
        } else {
            return false; // User no longer exists
        }
        return true; // All is good
    }

    private function ErrorInvalidUserPass() {
        sleep(2); // To avoid brute-force attachs (max-childs in apache config will ensure that very few requests can go through
        error(401, 'Invalid email or password');
    }
}
