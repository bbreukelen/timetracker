<?php
////////////////////////////////////////////////////////////////////////////////////////////////////
// This handles all incoming api requests. Requests are done using a folder-structure.
// Folders are v1/branch/request and those are converted using rewrite engine in .htaccess.
// All requests get here in api.php with category and request as GET parameters
////////////////////////////////////////////////////////////////////////////////////////////////////
include '../../../libs/config.php';
include $config['libspath'].'/simplepdo/simplePDO.class.php';
include $config['libspath'].'/simplejwt/simpleJWT.class.php';
include $config['libspath'].'/timeTracker.class.php';

$db = new simplePDO();
$main = new timeTracker;
$jwt = new simpleJWT();

// Setup external authentication checking - for every jwt-authenticate this will also be checked
include 'authentication.class.php';
$auth = new authentication();
$jwt->setExternalAuth($auth, 'confirmToken');

////////////////////////////////////////////////////////////////////////////////////////////////////

$category = filter_input(INPUT_GET, 'category');
$request  = filter_input(INPUT_GET, 'request');
error_log("Received REST API call: {$category} :: {$request}");
if ($category == null || $request == null) { error(400, "No category or request provided"); }

$categoryFile = preg_replace("|[^a-zA-Z_]|s", "", "{$category}") . ".class.php"; // Remove everything but a-z, A-Z and _ for security
if (! file_exists($categoryFile)) { error(400, "Invalid category"); }
include_once "./{$categoryFile}";
$className = $category;
$apiCat = new $className();

if (! method_exists($apiCat, $request)) { error(00, "Invalid request"); }

call_user_func( [ $apiCat, $request ] ); // Run the requested function in the api Class

///////////////////////////////////////////////////////////////////////////////////////////////////
function getParam($param) {
	global $input;
	if (! isset($input)) { // Cache input in array
		parse_str(file_get_contents("php://input"), $input);
	}

	if (! isset($input[$param])) { return null; }
	return $input[$param];
}

function success($data) {
    // Returns a 200 with requested data
    header('HTTP/1.0 200 OK');
    header('Content-type: application/json');
    print json_encode($data, JSON_NUMERIC_CHECK);
    exit(0);
}

function error($code, $message) {
    // Returns an error with a message
    switch ($code) {
        case 400: header('HTTP/1.0 400 Bad Request'); break;
        case 401: header('HTTP/1.0 401 Unauthorized'); break;
        case 403: header('HTTP/1.0 403 Forbidden'); break;
        case 405: header('HTTP/1.0 405 Method Not Allowed'); break;
        case 409: header('HTTP/1.0 409 Conflict'); break;
        default: header('HTTP/1.0 400 Bad Request'); break;
    }
    if ($message != null) { print $message; }
    exit(0);
}

function checkRole($session, $role) {
    if ($session->role == 'admin' || $session->role == $role) { return true; }
    error(403, 'You do not have sufficient privileges for this request');
}

function checkMethod($methodReq) {
    $method = (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']) ? $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'] : $_SERVER['REQUEST_METHOD']);
    if (strtoupper($method) == strtoupper($methodReq)) { return true; }
    error(405, 'Method not supported for this request');
}
