<?php
// Handles all reporting functions

class timereporting {

    public function __construct() { }

    public function listitems() {
        // Lists records for one day
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('GET');

        $ret = $this->doListItems($session);
        success($ret);
    }

    public function listdays() {
        // Lists records for multiple days
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('GET');

        $ret = $this->doListDays($session);
        success($ret);
    }

    public function exportdays() {
        // Lists records for multiple days
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('GET');

        $this->doExportDays($session);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function doListItems($session) {
        global $db;

        $user = filter_input(INPUT_GET, 'user');
        $date = filter_input(INPUT_GET, 'date');
        if ($date == null) { error(400, "Date is not specified"); }

        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        // Get items
        $sql1 = $db->prepare("SELECT id, user_id, `date`, hours, description, created_date FROM hours WHERE user_id = ? AND `date` = ? ORDER BY created_date ASC");
        $sql1->execute([$user, $date]);
        $items = $sql1->fetchAll();

        // Get totals etc
        $sql2 = $db->prepare("SELECT `date`, SUM(hours) total_hours
                              FROM hours WHERE hours.user_id = ? AND `date` = ? GROUP BY `date`");
        $sql2->execute([$user, $date]);
        $totals = $sql2->fetch();

        if (gettype($totals) == 'boolean' && $totals == false) {
            $totals = [
                'date' => $date,
                'total_hours' => 0
            ];
        }

        return [ 'summary' => $totals, 'details' => $items ];
    }

    private function doListDays($session) {
        global $db;
        $user = filter_input(INPUT_GET, 'user');
        $dateStart = filter_input(INPUT_GET, 'date_start');
        $dateEnd = filter_input(INPUT_GET, 'date_end');

        if ($dateStart == null) { error(400, "Date start is not specified"); }
        if ($dateEnd == null) { error(400, "Date end is not specified"); }

        if ($user != null && $user != $session->userid) {
            checkRole('admin');
        } else {
            $user = $session->userid;
        }

        // Get rollup data
        $sql = $db->prepare("SELECT `date`, SUM(hours) hours
                              FROM hours WHERE hours.user_id = ? AND `date` >= ? AND `date` <= ? GROUP BY `date` ORDER BY date ASC");
        $sql->execute([$user, $dateStart, $dateEnd]);
        $data = $sql->fetchAll();

        return $data;
    }

    private function doExportDays($session) {
        global $db, $main;
        $user = filter_input(INPUT_GET, 'user');
        $dateStart = filter_input(INPUT_GET, 'date_start');
        $dateEnd = filter_input(INPUT_GET, 'date_end');
        $format = filter_input(INPUT_GET, 'format');

        if ($dateStart == null) {
            error(400, "Date start is not specified");
        }
        if ($dateEnd == null) {
            error(400, "Date end is not specified");
        }

        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        // Get rollupdata
        $sql1 = $db->prepare("SELECT `date`, DATE_FORMAT(`date`,'%d.%m') date_human, SUM(hours) hours FROM hours
                              WHERE user_id = ? AND `date` >= ? AND `date` <= ? GROUP BY `date` ORDER BY `date` ASC");
        $sql1->execute([$user, $dateStart, $dateEnd]);
        $totals = $sql1->fetchAll();

        // Get details
        $sql2 = $db->prepare("SELECT `date`, description FROM hours WHERE user_id = ? AND `date` >= ? AND `date` <= ? ORDER BY `date` ASC, created_date ASC");
        $sql2->execute([$user, $dateStart, $dateEnd]);
        $details = $sql2->fetchAll();

        // Combine the data into a single object
        $o = new stdClass();
        $o->days = $totals;
        $o->notes = [];
        foreach ((array)$details as $detail) {
            if (!isset($o->notes[$detail->date])) {
                $o->notes[$detail->date] = [];
            }
            array_push($o->notes[$detail->date], $detail->description);
        }

        // Now convert the data to an HTML output
        $cont = '';
        foreach ((array)$o->days as $day) {
            $cont .= "<div class='date'>Date: {$day->date_human}</div>";
            $cont .= "<div class='time'>Total time: " . $this->humanHours($day->hours) . "</div>";
            $cont .= "<div class='notes'>Notes:</div>\n<ul>\n";
            foreach ($o->notes[$day->date] as $note) {
                $cont .= "\t<li class='note'>{$note}</li>";
            }
            $cont .= "</ul>\n";
        }

        $html = $main->getMailTemplate('htmlExport', [ 'data' => $cont]);

        if ($format != null) {
            switch ($format) {
                case 'base64' : success( ['html' => base64_encode($html)] ); break;
                case 'link'   : $this->makeExportLink($html); break;
            }
        }

        success( ['html' => $html ]);
    }

    private function makeExportLink($html) {
        global $config;
        $id = time() . rand(10000,99999);
        $path = "/tmp/{$id}";
        file_put_contents($path, $html);
        success( ['link' => $config['webpath'].'/dl.php?export='.$id ] );
    }

    private function humanHours($hours) {
        // Convert string of hours to 09h 15m
        $hours = (double) $hours;
        $minutes = $hours * 60;
        $Fhours = floor($minutes / 60);
        $Fmins = round($minutes - ($Fhours * 60),0);
        return "{$Fhours}h {$Fmins}m";
    }
}
