<?php
// Handles all time tracking functions

class timetracking {

    public function __construct() { }

    public function gettime() {
        // Get 1 time record
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('GET');

        $timeid = filter_input(INPUT_GET, 'id');
        if (!$timeid) { error(400, "Invalid record id"); }

        $time = $this->doGetTime($session, $timeid);
        if ($time->id != $timeid) { error(400, "Record does not exist"); }

        // Make sure you're allowed to view this record
        if ($time->user_id != $session->userid) {
            checkRole($session, 'admin');
        }

        success($time);
    }

    public function addtime() {
        // Add new time record
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('PUT');

        $newRecord = $this->doAddTime($session);
        success($newRecord);
    }

    public function edittime() {
        // Edit existing time record
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('PATCH');

        $timeid = getParam( 'id');
        $modRecord = $this->doUpdateTime($session, $timeid);
        success($modRecord);
    }

    public function deletetime() {
        // Deletes a record
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('DELETE');

        $timeid = filter_input(INPUT_GET, 'id');
        $this->doDeleteTime($session, $timeid);
        success([]);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function doAddTime($session) {
        // Creates a new time record
        global $db;

        $user   = getParam( 'user');
        $date   = getParam( 'date');
        $hours  = (double) getParam( 'hours');
        $descr  = getParam( 'description');

        // Validation checking
        if ($date == null) { error(400, "Date not provided"); }
        if ($hours == null) { error(400, "Hours not provided or incorrect format"); }
        if ($hours > 24) { error(400, "You cannot specify more than 24 hours"); }
        if ($descr == null) { error(400, "Description not provided"); }

        // Validate date format
        if (!$this->dateValidate($date)) { error(400, "Date is incorrect yyyy-mm-dd required"); }

        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        try {
            $newId = $db->uuid();
            $sql = $db->prepare("INSERT INTO `hours` (id, user_id, `date`, `hours`, description, created_date)
                                VALUES (?, ?, ?, ?, ?, NOW())");
            $sql->execute([$newId, $user, $date, $hours, $descr]);
            if (! $sql->rowCount()) {
                error_log("timeadd - Insert of new time record did not succeed 1");
                error(400, "Time record not added. Please contact us.");
            }

            return $this->doGetTime($session, $newId);

        } catch (Exception $e) {
            error_log("timeadd - Insert of new time record did not succeed 2");
            error(400, "Time record not added. Please contact us.");
        }
    }

    private function doGetTime($session, $timeid) {
        // Retrieve all details about one time record
        global $db;

        $user = filter_input(INPUT_GET, 'user'); if ($user == null) { $user = getParam('user'); }
        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        $sql = $db->prepare("SELECT id, user_id, `date`, hours, description, created_date FROM hours WHERE user_id = ? AND id = ?");
        $sql->execute([$user, $timeid]);
        return $sql->fetch();
    }

    private function doUpdateTime($session, $timeid) {
        // Updates existing user record
        global $db;

        $user = getParam( 'user');
        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        if (!$timeid) { error(400, "No record specified"); }

        $time = $this->doGetTime($session, $timeid);
        if ($time->id != $timeid) { error(400, "Record does not exist"); }

        // Make sure you're allowed to modify this record
        if ($time->user_id != $session->userid) {
            checkRole($session, 'admin');
        }

        $date   = getParam( 'date');
        $hours  = (double) getParam( 'hours');
        $descr  = getParam( 'description');

        // Validation checking
        if ($date == null && $hours == null && $descr == null) { error(400, "No update data specified"); }

        // Make update statement
        $upd = $db->update('hours', "user_id = ? AND id = ?", [ $user, $timeid ]);

        // Change date
        if ($date != null) {
            if (!$this->dateValidate($date)) { error(400, "Date is incorrect yyyy-mm-dd required"); }
            $upd->addField('date', $date);
        }

        // Change hours
        if ($hours != null) {
        		if ($hours > 24) { error(400, "You cannot specify more than 24 hours"); }
            $upd->addField('hours', $hours);
        }

        // Change description
        if ($descr != null) {
            $upd->addField('description', $descr);
        }

        $sql = $upd->update();

        if ($sql == null) {
            error_log("timeupdate - Error updating time record {$timeid}");
            error(400, "Time record update failed.");
        }

        return $this->doGetTime($session, $timeid);
    }

    private function doDeleteTime($session, $timeid) {
        // Deletes existing record
        global $db;

        $user = filter_input(INPUT_GET, 'user');
        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        // Validation checking
        if (!$timeid) { error(400, "No time record specified"); }

        $time = $this->doGetTime($session, $timeid);
        if ($time->id != $timeid) { error(400, "Record does not exist"); }

        // Make sure you're allowed to modify this record
        if ($time->user_id != $session->userid) {
            checkRole($session, 'admin');
        }

        try {
            $sql = $db->prepare("DELETE FROM hours WHERE user_id = ? AND id = ?");
            $sql->execute([$user, $timeid]);
            if (! $sql->rowCount()) {
                error_log("timedel - Error deleting time record {$timeid} -1");
                error(400, "Time deletion failed.");
            }

        } catch (Exception $e) {
            error_log("timedel - Error deleting time record {$timeid} -2");
            error(400, "Time deletion failed.");
        }
        return true;
    }

    private function dateValidate($date) {
        return preg_match('#^(?P<year>\d{4})([-/.])(?P<month>\d{1,2})\2(?P<day>\d{1,2})$#', $date, $matches) && checkdate($matches['month'], $matches['day'], $matches['year']);
    }
}
