<?PHP
// Handles all user management function
include_once $config['libspath'] . '/password.php'; // Needed because of old PHP version

class usermanagement {

    public function __construct() { }

    public function getsession() {
        // Retrieve session info required for web-frontend
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('GET');
        success(['email' => $session->email, 'role' => $session->role]);
    }

    public function register() {
        // Register yourself as new user
        global $jwt, $db;
        checkMethod('POST');

        $this->doRegistration();
        success([]);
    }

    public function passwordresetrequest() {
        // Send reset password mail
        global $jwt, $db;
        checkMethod('POST');

        $this->doResetSend();
        success([]);
    }

    public function passwordreset() {
        // Reset your password
        global $jwt, $db;
        checkMethod('POST');

        $this->doResetSave();
        success([]);
    }

    public function verifyemail() {
        // Verify email address after registration
        global $jwt, $db;
        checkMethod('GET');

        $key = filter_input(INPUT_GET, 'key');
        $this->doVerifyEmail($key);
        success([]);
    }

    public function changepassword() {
        // Change your password
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('PATCH');

        $userid = getParam('id');
        if (! $userid) { $userid = $session->userid; }
        if ($userid != $session->userid) {
            checkRole($session, 'usermanager');
        }

        $this->doChangePassword($userid);
        success([]);
    }


    //// Functions for usermanagers and admins

    public function listusers() {
        // Lists all users available with all their data
        global $jwt, $db;
        $session = $jwt->authenticate();
        checkMethod('GET');
        checkRole($session, 'usermanager');

        $sql = $db->prepare("SELECT id, email, role, email_validated, created_date, lastlogin_date FROM users ORDER BY email ASC");
        $sql->execute();
        $results = $sql->fetchAll();
        success($results);
    }

    public function adduser() {
        // Lists all users available with all their data
        global $jwt, $db;
        $session = $jwt->authenticate();
        checkMethod('PUT');
        checkRole($session, 'usermanager');

        $email = getParam('email');
        $passw = getParam('password');
        $role  = getParam('role');
        $newUser = $this->doAddUser($email, $passw, $role, false);

        if ($newUser->email == $email) {
            success($newUser);

        } else {
            error(400, "Unable to add user");
        }
    }

    public function edituser() {
        // Lists all users available with all their data
        global $jwt, $db;
        $session = $jwt->authenticate();
        checkMethod('PATCH');
        checkRole($session, 'usermanager');

        $userid = getParam('id');
        $updUser = $this->doUpdateUser($userid);
        success($updUser);
    }

    public function deleteuser() {
        // Deletes a record
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('DELETE');
        checkRole($session, 'usermanager');

        $userid = filter_input(INPUT_GET, 'id');
        $this->doDeleteUser($userid);
        success([]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function doRegistration() {
        $email = strtolower(getParam('email')); // NOTE: strtolower
        $passw = getParam('password');

        // Make user record
        $this->doAddUser($email, $passw, 'user', true); // True is (needs to be validated)
        error_log("User account created for {$email}");

        $this->doSendVerificationEmail($email);
        success([]);
    }

    private function doAddUser($email, $passw, $role, $needsValidation) {
        // Creates a new user record and returns the record
        global $db;

        // Validation checking
        if (!$email) { error(400, "No email specified"); }
        if (strlen($email) < 7) { error(40, "Invalid email address"); }
        if ($passw == null || strlen($passw) < 6) { error(400, "Password to short"); }
        if (! in_array($role, [ 'user', 'usermanager', 'admin'])) { error(400, "Invalid role"); }

        try {
            $newId = $db->uuid();
            $validationCode = ($needsValidation ? $db->uuid() : '');
            $validated = ($needsValidation ? 0 : 1);
            $passHashed = $this->hashPassword($passw);

            $sql = $db->prepare("INSERT INTO `users` (id, email, role, password, validation_code, email_validated, created_date)
                                VALUES (?, ?, ?, ?, ?, ?, NOW())");
            $sql->execute([$newId, $email, $role, $passHashed, $validationCode, $validated]);
            if (! $sql->rowCount()) {
                error_log("useradd - Insert of new user did not succeed 1");
                error(400, "User registration failed. Please contact us.");
            }

            // Add dummy preferences record
            try {
                $pref = $db->prepare("INSERT INTO preferences (user_id) VALUES (?)");
                $pref->execute([$newId]);
            } catch (Exception $e) {}

            return $this->doGetUser($newId);

        } catch (Exception $e) {
            error_log("useradd - Insert of new user did not succeed 2");
            error(409, "User already exists.");
        }
    }

    private function doUpdateUser($userid) {
        // Updates existing user record
        global $db;

        if (!$userid) { error(400, "No user specified"); }

        $user = $this->doGetUser($userid);
        if ($user->id != $userid) { error(400, "User does not exist"); }

        $emailN = getParam('email');
        $roleN = getParam('role');
        $passN = getParam('password');
        if ($roleN == null && $passN == null && $emailN == null) { error(400, "No update data specified"); }

        // Make update statement
        $upd = $db->update('users', "id = ?", [ $userid ]);

        // Change role
        if ($roleN != null) {
            if (! in_array($roleN, ['user', 'admin', 'usermanager'])) { error(400, "Invalid role specified"); }
            $upd->addField('role', $roleN);
        }

        // Change password
        if ($passN != null) {
            if (strlen($passN) < 6) { error(400, "Password too short"); }
            $upd->addField('password', $this->hashPassword($passN));
        }

        // Change email
        if ($emailN != null) {
            if (strlen($emailN) < 7) { error(400, "Invalid email address"); }
            $upd->addField('email', $emailN);
            $getEmail = $emailN;
        }

        $sql = $upd->update();

        if ($sql == null) {
            error_log("userupd - Error updating user {$userid}");
            error(400, "User update failed.");
        }

        return $this->doGetUser($userid);
    }

    private function doChangePassword($userid) {
        // Updates existing user record but only password
        global $db;
        if (!$userid) { error(400, "No user specified"); }
        $passN = getParam('password');
        if ($passN == null) { error(400, "No password specified"); }
        if (strlen($passN) < 6) { error(400, "Password too short"); }

        // Make update statement
        $upd = $db->update('users', "id = ?", [ $userid ]);
        $upd->addField('password', $this->hashPassword($passN));
        $sql = $upd->update();

        if ($sql == null) {
            error_log("userupd - Error changing password {$userid}");
            error(400, "Password change failed.");
        }
    }

    private function doDeleteUser($userid) {
        // Deletes existing user record
        global $db;

        // Validation checking
        if (!$userid) { error(400, "No user specified"); }

        try {
            $sql = $db->prepare("DELETE FROM users WHERE id = ?");
            $sql->execute([$userid]);
            if (! $sql->rowCount()) {
                error_log("userdel - Error deleting user {$userid} -1");
                error(400, "User deletion failed.");
            }

        } catch (Exception $e) {
            error_log("userdel - Error deleting user {$userid} -2");
            error(400, "User deletion failed.");
        }

        return true;
    }

    public function doGetUser($userid) {
        // Retrieve all details about one user record
        global $db;
        $sql = $db->prepare("SELECT id, email, role, email_validated, created_date, lastlogin_date FROM users WHERE id = ?");
        $sql->execute([$userid]);
        return $sql->fetch();
    }

    private function doSendVerificationEmail($email) {
        global $db, $config, $main;
        // Sends email with verification link to email address
        $sql = $db->prepare("SELECT validation_code FROM users WHERE email = ?");
        $sql->execute( [$email] );
        if (! ($row = $sql->fetch())) {
            error_log("sendVerificationEmail - Verification key for {$email} not found in database");
            return false;
        }
        $key = $row->validation_code;
        $mailData = Array( 'validation_url' => $config['webpath'] . "/verify/" . $key );
        $message = $main->getMailTemplate('registerThanks', $mailData);
        $main->sendEmail($email, "Time Tracker email verification", $message);
        error_log("Sent verification email.");
    }

    private function doVerifyEmail($key) {
        global $db;
        if (! $key) {
            $this->doVerifyEmailError();
            exit;
        }

        $sql = $db->prepare("SELECT email FROM users WHERE validation_code = ?");
        $sql->execute( [$key] );
        if (! $sql->rowCount()) { error_log("Verification key not found in database"); $this->doVerifyEmailError(); exit; }
        $email = $sql->fetchColumn(0);

        $sql = $db->prepare("UPDATE users SET email_validated = 1, validation_code = '' WHERE validation_code = ?");
        $sql->execute( [$key] );
        if ($sql->rowCount()) {
            error_log("Verification key removed for {$email}");
            $this->doVerifyEmailOk();
        } else {
            error_log("Error removing verification key for {$email}");
            $this->doVerifyEmailError();
        }
        exit;
    }

    private function doVerifyEmailError() {
        global $config;
        $_SESSION['loadMessage'] = "Your account has already been verified or your varificationcode is incorrect.";
        Header("Location: {$config['webpath']}/");
        exit(0);
    }

    private function doVerifyEmailOk() {
        global $config;
        $_SESSION['loadMessage'] = "Thank you, your email address has been verified. You can now sign in.";
        Header("Location: {$config['webpath']}/");
        exit(0);
    }

    private function hashPassword($pwd) {
        global $config;
        $start = microtime(true);
        $hash = password_hash($pwd, PASSWORD_BCRYPT, Array('cost' => $config['password_cost']) );
        $time = round(microtime(true) - $start,3);
        error_log("Created password in {$time} seconds");
        return $hash;
    }

    private function doResetSend() {
        global $db, $main;
        // When users forgot their password this is called to send a validation code
        // Check if exists and validated
        $email = getParam('email');
        if ($email == null) { error(400, "No email address provided"); }
        $sql = $db->prepare("SELECT id, email, email_validated FROM users WHERE email = ?");
        $sql->execute( [$email] );
        if (! $sql->rowCount()) { error(400, "Invalid email address or already validated."); }
        $data = $sql->fetch();

        if (! $data->email_validated) { error(400, "Could not reset your password. Please contact us"); }

        // Make & save validation code
        $code = strtoupper(uniqid());
        $sql = $db->prepare("UPDATE users SET validation_code = ?, reset_status = 'mail_sent' WHERE id = ?");
        $sql->execute( [$code, $data->id] );
        if (! $sql->rowCount()) { error(400, "Could not reset your password. Please contact us"); }

        // Send email
        $mailData = array_merge( (array) $data, Array('validation_code' => $code));
        $message = $main->getMailTemplate('passwordReset', $mailData);
        $main->sendEmail($data->email, "Time Tracker password reset", $message);

        error_log("Password reset requested for {$_POST['email']}. Email with code sent");
    }

    private function doResetSave() {
        global $db;
        // Saves new password in password reset
        $email = getParam('email');
        $code  = getParam('validationcode');
        $password  = getParam('password');
        if ($email == null || $code == null | $password == null) { error(400, "Could not reset password"); }

        $sql =  $db->prepare("SELECT id FROM users WHERE reset_status = 'mail_sent' AND validation_code IS NOT NULL AND validation_code = ? AND email = ?");
        $sql->execute( [$code, $email] );
        if (! $sql->rowCount()) { error(400, "Incorrect validation code retrieved"); }
        $data = $sql->fetch();

        // Now reset password
        $sql = $db->prepare("UPDATE users SET validation_code = '', reset_status = NULL, password = ? WHERE id = ?");
        $sql->execute( [$this->hashPassword($password), $data->id] );
        if (! $sql->rowCount()) { error(400, "Could not reset your password."); }

        error_log("Password reset completed for {$email}.");
    }

}
