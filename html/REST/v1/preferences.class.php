<?php
// Handles all preferences functions

class preferences {

    public function __construct() { }

    public function getpreferences() {
        // Returns preferences
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('GET');

        $ret = $this->doGetPreferences($session);
        success($ret);
    }

    public function savepreferences() {
        // Update preferences
        global $jwt;
        $session = $jwt->authenticate();
        checkMethod('PUT');

        $ret = $this->doSavePreferences($session);
        success($ret);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function doGetPreferences($session) {
        global $db;

        $user = filter_input(INPUT_GET, 'user'); if ($user == null) { $user = getParam('user'); }
        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        $sql = $db->prepare("SELECT workdays, workhours FROM preferences WHERE user_id = ?");
        $sql->execute([$user]);
        return $sql->fetch();
    }

    private function doSavePreferences($session) {
        global $db;

        $user = getParam('user');
        $workdays  =  getParam('workdays');
        $workhours = getParam('workhours');

        if ($user != null && $user != $session->userid) {
            checkRole($session, 'admin');
        } else {
            $user = $session->userid;
        }

        if ($workdays  == null) { error(400, "Workdays not specified"); }
        if ($workhours == null) { error(400, "Workhours not specified"); }
        if ($workhours > 24) { error(400, "You cannot have more than 24 hours in a working day"); }

        // Do replace
        $sql = $db->prepare("REPLACE INTO preferences (user_id, workdays, workhours) VALUES ( ?, ?, ? )");
        $sql->execute([$user, $workdays, $workhours]);

        return $this->doGetPreferences($session);
    }
}
