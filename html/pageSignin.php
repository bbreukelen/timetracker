<div class="mainTitle" id="signInTitle">Sign In</div>
<div>
    <div id="signInUserFieldC"><input type="text" class="formField" id="signInUserField" placeholder="e-mail address" /></div>
    <div id="signInPasswordFieldC">
        <input type="password" class="formField" id="signInPasswordField" placeholder="password" />
        <div id="signInRecoverLink">recover</div>
    </div>
    <div id="signInButtons">
        <div id="signInSignIn" class="button">sign in</div>
        <div id="signInRegister" class="button hidden">register</div>
        <div id="signInSignInLink" class="hidden noSelect">or sign in with your account</div>
        <div id="signInRegisterLink" class="noSelect">or register a new account</div>
        <div id="signInRecover" class="button hidden">recover</div>
        <div id="signInChange" class="button hidden">save password</div>
    </div>
</div>