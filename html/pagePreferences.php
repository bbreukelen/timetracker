<div class="mainTitle">Preferences</div>
<div>
    <div class="subTitle">Working days</div>
    <table border="0" class="prefsTable">
        <tbody>
        <tr>
            <td>Mon</td>
            <td>Tue</td>
            <td>Wed</td>
            <td>Thu</td>
            <td>Fri</td>
            <td>Sat</td>
            <td>Sun</td>
        </tr>
        <tr>
            <td><input type="checkbox" id="prefsCB1" class="prefsDay" /></td>
            <td><input type="checkbox" id="prefsCB2" class="prefsDay" /></td>
            <td><input type="checkbox" id="prefsCB3" class="prefsDay" /></td>
            <td><input type="checkbox" id="prefsCB4" class="prefsDay" /></td>
            <td><input type="checkbox" id="prefsCB5" class="prefsDay" /></td>
            <td><input type="checkbox" id="prefsCB6" class="prefsDay" /></td>
            <td><input type="checkbox" id="prefsCB0" class="prefsDay" /></td>
        </tr>
        </tbody>
    </table>

    <div class="subTitle">Working hours</div>
    <div><input type="text" id="prefsHours" class="formField"/></div>

    <div id="prefsSave" class="button">save</div>

</div>