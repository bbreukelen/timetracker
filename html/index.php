<? session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Time Tracker</title>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="keywords" content="time tracker, time, tracker">
    <meta name="description" content="Time Tracker application">
    <link rel="stylesheet" type="text/css" href="stylesheets/is-calendar.css" />
    <link rel="stylesheet" type="text/css" href="stylesheets/framework.css" />
    <link rel="stylesheet" type="text/css" href="stylesheets/pageSignin.css" />
    <link rel="stylesheet" type="text/css" href="stylesheets/pagePreferences.css" />
    <link rel="stylesheet" type="text/css" href="stylesheets/pageTracker.css" />
    <link rel="stylesheet" type="text/css" href="stylesheets/pageReports.css" />
    <link rel="stylesheet" type="text/css" href="stylesheets/pageUsers.css" />
    <script>window.msg = "<?=getSess('loadMessage')?>";</script>
    <script type="text/javascript" src="javascript/MooTools-More-1.6.0.js"></script>
    <script type="text/javascript" src="javascript/rest.js"></script>
    <script type="text/javascript" src="javascript/is-calendar.js"></script>
    <script type="text/javascript" src="javascript/framework.js"></script>
    <script type="text/javascript" src="javascript/datetime.js"></script>
    <script type="text/javascript" src="javascript/userPopup.js"></script>
    <script type="text/javascript" src="javascript/pageSignin.js"></script>
    <script type="text/javascript" src="javascript/pagePreferences.js"></script>
    <script type="text/javascript" src="javascript/pageTracker.js"></script>
    <script type="text/javascript" src="javascript/pageReports.js"></script>
    <script type="text/javascript" src="javascript/pageUsers.js"></script>
</head>
<body>
<div id='message'></div>
<div id='app'>
	<!--<div id='popBlocker'></div>-->

	<!-- HEADER -->
	<div id='header'>
        <div id="topBar"></div>
		<div id="logo" class="hidden"></div>
		<div id="topMenu">
			<div id="menuTracker" class="topMenuItem noSelect">Tracker</div>
			<div id="menuReports" class="topMenuItem noSelect">Reports</div>
		</div>
		<div id="headerUser" class="hidden"></div>
		<div id="headerUserEmail" class="hidden"></div>
	</div>

    <!-- Popup menu -->
    <div id="userPopup" class="hidden">
        <div id="userPopPreferences" class="userPopupItem">Preferences</div>
        <div id="userPopUserManagement" class="userPopupItem hidden">Manage Users</div>
        <div id="userPopChangePassword" class="userPopupItem">My Password</div>
        <div id="userPopStopImpersonating" class="userPopupItem hidden">Back to myself</div>
        <div id="userPopLogout" class="userPopupItem">Sign out</div>
    </div>

	<!-- CONTENT -->
	<div id="content">
        <div id="pageSignin" class="page">
            <? include 'pageSignin.php'?>
        </div>

        <div id="pagePreferences" class="page">
            <? include 'pagePreferences.php'?>
        </div>

        <div id="pageTracker" class="page">
            <? include 'pageTracker.php'?>
        </div>

        <div id="pageUsers" class="page">
            <? include 'pageUsers.php'?>
        </div>

        <div id="pageReports" class="page">
            <? include 'pageReports.php'?>
        </div>
    </div>
</div>
<!-- FOOTER -->
<div id='footer'>
    <div class='footMenuItem noSelect'>Time Tracker</div>
    <div class='footMenuItem noSelect'>Copyright 2016</div>
</div>
</body>
</html>
<?php
function getSess($var) {
    if (! $var) { return null; }
    if (! isset($_SESSION[$var])) { return null; }
    $ret = $_SESSION[$var];
    unset($_SESSION[$var]);
    return $ret;
}
