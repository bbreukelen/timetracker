<?php
// Used to download exported HTML file for front-end users
$file = filter_input(INPUT_GET, 'export');
$file = preg_replace("/\D/",'', $file); // Make sure only numbers can be part of the id for security reasons
$path = "/tmp/{$file}";
if (!$file || !file_exists($path)) {
    print "Non-existing export";
    exit(0);
}

header("Content-Type: text/html; charset=utf-8");
header("Content-Disposition: attachment; filename=export_{$file}.html");
readfile("/tmp/{$file}");
unlink($path);
