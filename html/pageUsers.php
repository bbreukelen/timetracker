<div class="mainTitle">User Management</div>
<div id="usersHeader">
    <div class="usersHEmail usersHeaderCol">email</div>
    <div class="usersHRole usersHeaderCol">role</div>
    <div class="usersHPassword usersHeaderCol">password</div>
    <div class="usersHValidated usersHeaderCol">validated</div>
    <div class="usersHCreated usersHeaderCol">created</div>
    <div class="usersHLastSignin usersHeaderCol">last signin</div>
    <div class="usersHImpersonate usersHeaderCol">impersonate</div>
</div>

<div id="usersList"></div>

<div id="usersAddNew">+</div>