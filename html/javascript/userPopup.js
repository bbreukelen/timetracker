app.userPopup = {
    visible: false,
    hovered: false
};

app.userPopup.init = function() {
    app.userPopup.el = $('userPopup');
    app.userPopup.handle = $('headerUser');
    app.userPopup.handle.addEvent('click', function(e) {
        if (app.userPopup.visible == true) {
            app.userPopup.hide();
        } else {
            app.userPopup.show();
        }
    });
    app.userPopup.el.addEvent('mouseover', function(e) {
        if (app.userPopup.hovered == false) { app.userPopup.hovered = true; }
    });
    app.userPopup.el.addEvent('mouseleave', function(e) {
        if (app.userPopup.hovered == true) { app.userPopup.hide(); }
    });

    app.userPopup.items.init();
};
app.userPopup.hide = function() {
    app.userPopup.el.hide();
    app.userPopup.visible = false;
    app.userPopup.hovered = false;
};
app.userPopup.show = function() {
    app.userPopup.visible = true;
    app.userPopup.hovered = false;
    app.userPopup.el.show();
};
app.userPopup.items = {};
app.userPopup.items.init = function() {
    $('userPopLogout').addEvent('click', function() {
        app.impersonate.stop();
        rest.logout();
        app.getSessionInfo(function() { app.showStartPage(); });
    });

    $('userPopUserManagement').addEvent('click', function(){ app.page.show('pageUsers'); });

    $('userPopStopImpersonating').addEvent('click', app.impersonate.stop.bind());

    $('userPopPreferences').addEvent('click', function(){ app.page.show('pagePreferences'); });

    $('userPopChangePassword').addEvent('click', function(){
        app.page.show('pageSignin');
        app.pageSignin.showChangePasswordFields();
    });
};