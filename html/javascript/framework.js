var app = {
    prefs: null
};
app.config = {
    'preloadImages':[ 'img/user_red.png', 'img/edit_active.png', 'img/delete_active.png' ]
};

app.init = function() {
    // Rest config
    rest.config.setRestPath('/REST/v1');
    rest.config.setDefaultError(function(msg) { app.message.show(msg); });
    rest.config.setOnUnauthorized(app.onUnauthorized.bind());

    app.preloadImages(app.config.preloadImages);
    app.message.init();
    app.menu.init();
    app.userPopup.init();
    app.pageSignin.init();
    app.pagePreferences.init();
    app.pageTracker.init();
    app.pageReports.init();
    app.pageUsers.init();
    app.getSessionInfo(function() {
        if (window.msg != undefined && window.msg != '') { app.message.show(window.msg); }
        app.showStartPage();
    });
};

/* SESSION and AUTHENTICATION */
app.session = {};
app.getSessionInfo = function(onComplete) {
    rest.request('GET', 'usermanagement', 'getsession', {}, function(r) {
        r.loggedin = true;
        app.setSession(r);
        if (onComplete != undefined) { onComplete(); }
    }, function(r) {
        app.setSession({ loggedin: false });
        if (onComplete != undefined) { onComplete(); }
    });
};

app.setSession = function(sess) {
    app.session = sess;
    if (app.session != undefined && app.session.loggedin === true) {
        app.switchMenus(true);
    } else {
        app.switchMenus(false);
    }
};

app.switchMenus = function(loggedin) {
    // Loggedin / Loggedout stuff
    if (loggedin) {
        $('headerUser').show();
        $('headerUserEmail').set('html', (app.impersonate.user == null ? app.session.email : app.impersonate.user));
        $('headerUserEmail').show();

    } else {
        $('headerUser').hide();
        $('headerUserEmail').hide();
        $('headerUserEmail').set('html', '');
        app.userPopup.hide();
    }

    // Role-based stuff
    if (app.session.role != undefined && app.session.role == 'admin' || app.session.role == 'usermanager') {
        $('userPopUserManagement').show();
    } else {
        $('userPopUserManagement').hide();
        if (app.page.showing == 'pageUsers') { app.page.show('pageTracker'); }
    }

};
app.onUnauthorized = function() {
    if (app.page.showing != 'pageSignin') { app.page.show('pageSignin'); }
};

/* PAGES STUFF */
app.page = {};
app.page.showing = 'pageSignin';
app.page.show = function(pageId) {
    $$('div.page').hide();
    var page = $(pageId);
    if (page == null) { app.message.show('Unknown page'); return; }
    page.show();
    if (page.onOpen != undefined) { page.onOpen(); }
    app.page.showing = pageId;
};

app.showStartPage = function() {
    // What page should we start with?
    if (app.session == undefined || app.session.loggedin == false) {
        // Default to sign-in page
        app.page.show('pageSignin');
    } else {
        // Default to time tracker
        app.page.show('pageTracker');
    }
};


app.preloadImages = function(images) {
    if (typeOf(images) == 'string') { images = [images]; }
    if (typeOf(images) != 'array') { return; }
    Asset.images(images);
};

//// Part for showing screen errors ////
app.message = {};
app.message.init = function() {
    app.message.topMarge = 10; // For shadows and border etc
    app.message.timeout = 6; // Seconds
    app.message.txt = new Element('div', { 'class':'txt'}).inject($('message'));
    app.message.but = new Element('div', { 'class':'close'}).addEvent('click', app.message.close.bind()).inject($('message'));
    app.message.h = $('message').measure(function() { return (this.getSize())['y']; });
    $('message').setStyle('top', -app.message.h - app.message.topMarge);
    setTimeout(function() { $('message').show(); }, 500); // Prevent it from showing while still css animating closed
};
app.message.show = function(msg) {
    // Animation is handles with CSS3
    app.message.txt.innerHTML = msg;
    $('message').setStyle('top', 0);
    if (app.message.timer != undefined) { clearTimeout(app.message.timer); }
    app.message.timer = setTimeout(app.message.close, app.message.timeout * 1000);
};
app.message.close = function() {
    $('message').setStyle('top', -app.message.h - app.message.topMarge);
    if (app.message.timer != undefined) { clearTimeout(app.message.timer); }
};
//// End screen errors ////


//// Part about validating form fields ////
app.forms = {};
app.forms.fieldFeedback = function(field, message) {
    // Show error message next to field and indicate field in some color
    if ($(field.id+'Err') != null) { $(field.id+'Err').destroy(); }
    field.removeClass('fieldError');
    field.setAttribute('verified','false');
    if (field.getAttribute('type') == 'button') { field.disabled = false; }

    if (message == undefined || message == null || message == 0) { return; }

    // Verified icon
    if (message == 'ok') {
        field.setAttribute('verified','true');
        new Element('img',{
            id:field.id + 'Err',
            src:'img/verified.png',
            'class':'fieldVerified'
        }).inject(field,'after');
        return;
    }

    // Spinner
    if (message == 'spin') {
        new Element('img',{
            id:field.id + 'Err',
            src:'img/spinner16.gif',
            'class':'fieldSpinner'
        }).inject(field,'after');
        return;
    }

    if (message == 'butspin') {
        // For buttons only
        if (field.getAttribute('type') == 'button') { field.disabled = true; }
        app.addSpinner(field, field.id + 'Err', 'butSpinner');
        return;
    }

    // Error message
    new Element('span',{
        id:field.id + 'Err',
        'class':'fieldError',
        html:message
    }).inject(field,'after');
    field.addClass('fieldError');
};
app.forms.emailValid = function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
app.forms.verified = function(fields) {
    // Verifies an array of form fields to check if they have verified=true as attribute - returns true of false
    var verified = true;
    Array.each(fields, function(obj) {
        if (obj.getAttribute('verified') == 'true') { return; }
        obj.addClass('fieldError');
        verified = false;
    });
    return verified;
};
app.forms.setValue = function(obj, value) {
    if (typeOf(obj) == 'string') { obj = $(obj); }
    if (obj == undefined || obj == null) { return; }
    switch (obj.type) {
        case 'text': case 'textarea': case 'hidden': case 'password':
        if (obj.oldPlaceholder != undefined && value.length > 0) { obj.oldPlaceholder.deactivatePlaceholder(); } // This and the similar line below are for old browsers that don't support placeholders
        obj.value = value;
        if (obj.oldPlaceholder != undefined && value.length == 0) { obj.oldPlaceholder.activatePlaceholder(); }
        break;
        case 'select-one':
            for (i=0;i<obj.options.length;i++) {
                if (obj.options[i].value === value) { obj.selectedIndex = i; }
            }
            break;
    }
};
app.forms.getValue = function(obj) {
    // Pulls the value for any type of form field
    if (typeOf(obj) == 'string') { obj = $(obj); }
    if (obj == undefined || obj == null) { return; }
    switch (obj.type) {
        case 'text': case 'textarea': case 'hidden': case 'password':
        return obj.value;
        break;
        case 'select-one':
            return app.forms.selectValue(obj);
            break;
        case 'checkbox':
            return (obj.checked ? "1" : "0");
            break;
    }
};
app.forms.selectValue = function(obj) {
    // Returns selected value from select field
    return obj.options[obj.selectedIndex].value;
};
app.forms.addOption = function(obj, value, name) {
    // Add option to select field
    if (name == undefined) { name = value; }
    new Element('option', { 'text': name, 'value': value }).inject(obj);
};
app.forms.delOptions = function(obj, dflt) {
    obj.empty();
    if (dflt != undefined) {
        app.forms.addOption(obj, '', dflt);
        obj.setAttribute('default', '1');
        obj.options[0].setAttribute('disabled','true');
        obj.options[0].setAttribute('selected','true');
    }
};
app.forms.initSelect = function() {
    $$('select').addEvent('change', function() {
        this.setAttribute('default', (app.forms.selectValue(this) == '' ? '1' : '0') );
    });
};
app.forms.clearAll = function(container) {
    // Clears every single form field inside the container
    if (typeOf(container) == 'string') { container = $(container); }
    if (container == undefined || container == null) { return; }
    container.getElements('input, select, textarea').each(function(inp) { app.forms.setValue(inp,''); });
};
app.forms.checkPattern = function(obj) {
    if (typeOf(obj) == 'string') { obj = $(obj); }
    var pattern = obj.getAttribute('apppattern');
    if (pattern == null) { return; }
    pattern = '^' + pattern + '{1,}$';
    var regex = new RegExp(pattern, "g");
    var value = app.forms.getValue(obj);
    if (value.length < 1) {
        app.forms.fieldFeedback(obj);
        return;
    }
    if (regex.test(value)) {
        app.forms.fieldFeedback(obj);
        return true;
    } else {
        app.forms.fieldFeedback(obj, "Invalid value provided");
        return false;
    }
};
app.button = {};
app.button.working = function(obj) {
    // Shows spinner on button - note, it doesn't disable it since it's not truly a button. use app.button.onclick
    if (typeOf(obj) == 'string') { obj = $(obj); }
    if (obj == null) { return; }
    obj.setAttribute('working', 'yes');
};
app.button.done = function(obj) {
    // Removed spinner and enables it
    if (typeOf(obj) == 'string') { obj = $(obj); }
    if (obj == null) { return; }
    obj.setAttribute('working', 'no');
};
app.button.onclick = function(obj, fn) {
    // Add click listener to button which is disabled when button is working
    if (typeOf(obj) == 'string') { obj = $(obj); }
    if (obj == null) { return; }
    obj.addEvent('click', function(e) {
        if (e.target.getAttribute('working') == 'yes') { return; }
        fn(e);
    });
};
//// End field validation part ////

app.addSpinner = function(obj, id, cl) {
    if (id == undefined) { return; }
    if (cl == undefined) { cl = ''; }
    return new Element('span', { 'class' : 'spinner '+cl, 'id' : id }).inject(obj, 'after');
};

app.menu = {};
app.menu.init = function() {
    $('menuTracker').addEvent('click', function() {
        if (app.session != undefined && app.session.loggedin === true) {
            app.page.show('pageTracker');
        } else {
            app.message.show("You need to be signed in to track time");
        }
    });

    $('menuReports').addEvent('click', function() {
        if (app.session != undefined && app.session.loggedin === true) {
            app.page.show('pageReports');
        } else {
            app.message.show("You need to be signed in to view time reports");
        }
    });
};

app.impersonate = {
    userid: null,
    email: null
};
app.impersonate.start = function(id, email) {
    if (email == app.session.email) {
        app.message.show("You cannot impersonate yourself");
        return;
    }
    app.impersonate.userid = id;
    app.impersonate.email = email;
    app.impersonate.updateUI();
    app.page.show('pageTracker');
};
app.impersonate.stop = function() {
    app.impersonate.userid = null;
    app.impersonate.email = null;
    app.impersonate.updateUI();
    app.page.show('pageTracker');
};
app.impersonate.updateUI = function() {
    $('headerUserEmail').set('html', (app.impersonate.userid == null ? app.session.email : app.impersonate.email));
    if (app.impersonate.userid == null) {
        $('userPopStopImpersonating').hide();
    } else {
        $('userPopStopImpersonating').show();
    }
    app.pageTracker.getPrefs();
};
app.impersonate.getUser = function() {
    // Returns impersonated user if any or else returns loggedin user
    return (app.impersonate.userid != null ? app.impersonate.userid : null);
};

window.addEvent('domready', function() { app.init(); });


