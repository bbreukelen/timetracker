app.pageUsers = { };
app.pageUsers.init = function() {
    $('pageUsers').onOpen = app.pageUsers.onOpen.bind();

    $('usersAddNew').addEvent('click',  app.pageUsers.addNew.bind());
};
app.pageUsers.onOpen = function() {
    // Load list and reset some stuff here I presume
    app.pageUsers.load();
};

app.pageUsers.clean = function() {
    $('usersList').set('html','');
};
app.pageUsers.load = function() {
    app.pageUsers.clean();

    $('usersList').setAttribute('working', 'yes');
    rest.request('GET', 'usermanagement', 'listusers', {}, function(r) {
        $('usersList').setAttribute('working', 'no');
        app.pageUsers.showList(r);
    }, function(err) {
        app.message.show(err);
        $('usersList').setAttribute('working', 'no');
    });
};
app.pageUsers.showList = function(r) {
    if (typeof(r) == 'object') {
        Array.each(r, function(row) {
            app.pageUsers.addRow(row);
        });
    }
};
app.pageUsers.addRow = function(row) {
    var c = new Element('div', { 'class': 'usersItem', 'id': row.id  });
    new Element('div', { 'class': 'usersItemEdit' }).addEvent('click', app.pageUsers.editRow.bind()).inject(c);
    new Element('div', { 'class': 'usersItemDelete' }).addEvent('click', app.pageUsers.deleteRow.bind()).inject(c);
    new Element('div', { 'class': 'usersItemCol usersItemEmail', 'html':row.email }).inject(c);
    new Element('div', { 'class': 'usersItemCol usersItemRole', 'text':row.role }).inject(c);
    new Element('div', { 'class': 'usersItemCol usersItemPassword', 'html':'___' }).inject(c);
    new Element('div', { 'class': 'usersItemCol usersItemValidated', 'text':app.pageUsers.validated(row.email_validated) }).inject(c);
    new Element('div', { 'class': 'usersItemCol usersItemCreated', 'text':app.dt.humanDate(row.created_date, app.dt.TYPE_DATE_SHORT) }).inject(c);
    new Element('div', { 'class': 'usersItemCol usersItemLastSignin', 'text':app.dt.humanDate(row.lastlogin_date, app.dt.TYPE_DATE_SHORT) }).inject(c);
    new Element('div', { 'class': 'usersItemCol usersItemImpersonate', 'html':(app.session.role == 'admin' ? 'impersonate' : '&nbsp;')})
        .addEvent('click', app.impersonate.start.bind(this, row.id, row.email)).inject(c);
    new Element('div', { 'class': 'usersItemSave usersItemBut', 'text':'save' }).inject(c);
    new Element('div', { 'class': 'usersItemUndo usersItemBut', 'text':'undo' }).inject(c);
    c.inject($('usersList'));
    return c;
};
app.pageUsers.editRow = function(eOrId) {
    var id = (typeof(eOrId) == 'object' ? eOrId.target.getParent().id : eOrId);
    app.pageUsers.itemToEditor($(id));
};
app.pageUsers.deleteRow = function(eOrId) {
    var id = (typeof(eOrId) == 'object' ? eOrId.target.getParent().id : eOrId);

    if (confirm("Delete this user, really????")) {
        app.pageUsers.del(id);
    }
};
app.pageUsers.itemToEditor = function(obj) {
    var e = obj.getElements('div.usersItemEmail')[0];
    e.setAttribute('lastvalue', e.get('text'));
    var eI = new Element('input', { 'type':'text', 'class': 'usersItemFld usersItemEmail' });
    eI.value = e.get('text');
    e.innerHTML = '';
    eI.inject(e);

    var r = obj.getElements('div.usersItemRole')[0];
    r.setAttribute('lastValue', r.get('text'));
    var rS = new Element('select', { 'class': 'usersItemFld usersItemRole' });
    app.forms.addOption(rS, 'user');
    app.forms.addOption(rS, 'usermanager');
    app.forms.addOption(rS, 'admin');
    app.forms.setValue(rS, r.get('text'));
    r.innerHTML = '';
    rS.inject(r);

    var p = obj.getElements('div.usersItemPassword')[0];
    var pI = new Element('input', { 'type':'password', 'class': 'usersItemFld usersItemPassword' });
    p.innerHTML = '';
    pI.inject(p);

    obj.getElements('div.usersItemImpersonate')[0].hide();

    obj.getElements('div.usersItemUndo')[0].addEvent('click', function(e) {
        // On Undo
        var ref = e.target.getParent();
        var email = ref.getElements('div.usersItemEmail')[0].getAttribute('lastvalue');
        var role = ref.getElements('div.usersItemRole')[0].getAttribute('lastvalue');
        app.pageUsers.itemToNormal(ref, email, role);
    }).show();

    obj.getElements('div.usersItemSave')[0].addEvent('click', function(e) {
        // On Save
        var ref = e.target.getParent();
        var id = ref.id;
        var email = ref.getElements('input.usersItemEmail')[0].value;
        var role = app.forms.getValue(ref.getElements('select.usersItemRole')[0]);
        var password = ref.getElements('input.usersItemPassword')[0].value;
        app.pageUsers.save(ref, id, email, role, password);
    }).show();
};
app.pageUsers.itemToNormal = function(obj, email, role) {
    if (obj.id == '') { obj.destroy(); return; } // For undoing new
    var e = obj.getElements('div.usersItemEmail')[0];
    e.innerHTML = email;
    e.setAttribute('lastvalue','');

    var r = obj.getElements('div.usersItemRole')[0];
    r.innerHTML = role;
    r.setAttribute('lastvalue','');

    var p = obj.getElements('div.usersItemPassword')[0];
    p.innerHTML = '___';

    obj.getElements('div.usersItemImpersonate')[0].show();
    obj.getElements('div.usersItemUndo')[0].removeEvents().hide();
    obj.getElements('div.usersItemSave')[0].removeEvents().hide();
};

app.pageUsers.save = function(obj, id, email, role, password) {
    if (id == null || id == '') {
        rest.request('PUT', 'usermanagement', 'adduser', {
            'email': email,
            'role': role,
            'password': password
        }, function (r) {
            obj.id = r.id;
            app.pageUsers.itemToNormal(obj, r.email, r.role);
        });

    } else {
        rest.request('PATCH', 'usermanagement', 'edituser', {
            'id': id,
            'email': email,
            'role': role,
            'password': password
        }, function (r) {
            app.pageUsers.itemToNormal($(id), r.email, r.role);
            if (email == app.session.email) {
                app.getSessionInfo();
            }
        });
    }
};
app.pageUsers.del = function(id) {
    rest.request('DELETE', 'usermanagement', 'deleteuser', {'id':id}, function(r) {
        $(id).destroy();
    });
};

app.pageUsers.addNew = function() {
    var newRec = app.pageUsers.addRow({
        'id':'',
        "email":"&nbsp;",
        "role":"user",
        "email_validated": 1,
        "created_date": app.dt.humanDate(new Date(), app.dt.TYPE_DATE_SHORT),
        "lastlogin_date": ''
    });
    app.pageUsers.itemToEditor(newRec);
};
app.pageUsers.validated = function(val) {
    return (val ? 'Yes' : 'No');
};
