app.pageTracker = { };
app.pageTracker.init = function() {
    $('pageTracker').onOpen = app.pageTracker.onOpen.bind();

    app.pageTracker.date = app.dt.today();

    $('trackYesterday').addEvent('click', app.pageTracker.dayAdd.bind(this, -1));
    $('trackTomorrow').addEvent('click',  app.pageTracker.dayAdd.bind(this, 1));
    $('trackAddNew').addEvent('click',  app.pageTracker.addNew.bind());

    $('trackDate').makeCalendar({
        'position': 'center',
        'caption_color': '#777777',
        'week_color': '#777777',
        'format': '%Y-%m-%d',
        value: app.pageTracker.date,
        'onSelectDate': function (dt) {
            app.pageTracker.date = dt;
            app.pageTracker.onOpen();
        }
    });
};
app.pageTracker.onOpen = function() {
    // Load list and reset some stuff here I presume
    app.pageTracker.showDate();
    if (app.prefs == null) {
        app.pageTracker.getPrefs(app.pageTracker.loadDay.bind());
    } else {
        app.pageTracker.loadDay();
    }
};

app.pageTracker.getPrefs = function(fn) {
    // Pull prefs on open and execute some function after if needed
    rest.request('GET', 'preferences', 'getpreferences', { 'user': app.impersonate.getUser() }, function(p) {
				p.workdays = p.workdays.toString(); // PHP converts this to int if only 1 day selected
        app.prefs = p;
        if (fn != undefined) { fn(); }
    });
};

app.pageTracker.showDate = function() {
    $('trackDateText').set('text', app.dt.humanDate(app.pageTracker.date, app.dt.TYPE_DATE_LONG));
};
app.pageTracker.dayAdd = function(step) {
    app.pageTracker.date = app.dt.addDay(app.pageTracker.date, step);
    app.pageTracker.onOpen();
};

app.pageTracker.clean = function() {
    $('trackTotalHours').set('text','');
    $('trackList').set('html','');
};
app.pageTracker.loadDay = function() {
    app.pageTracker.clean();

    $('trackList').setAttribute('working', 'yes');
    rest.request('GET', 'timereporting', 'listitems', { 'user':app.impersonate.getUser(), 'date':app.pageTracker.date }, function(r) {
        $('trackList').setAttribute('working', 'no');
        app.pageTracker.showTotal(r);
        app.pageTracker.showList(r);
    }, function(err) {
        app.message.show(err);
        $('trackList').setAttribute('working', 'no');
    });
};
app.pageTracker.reloadTotal = function() {
    rest.request('GET', 'timereporting', 'listitems', { 'user':app.impersonate.getUser(), 'date':app.pageTracker.date }, function(r) {
        app.pageTracker.showTotal(r);
    });
};
app.pageTracker.showTotal = function(r) {
    var tot = '00:00';
    if (r.summary != undefined && r.summary.total_hours != undefined) {
        tot = app.dt.humanTime(r.summary.total_hours);
    }
    $('trackTotalHours').set('text', tot);

    // Shortage?
    var hours = app.dt.dbTime(tot);
    $('pageTracker').setAttribute('hourshortage', app.pageTracker.hourShortage(app.pageTracker.date, hours));
};
app.pageTracker.showList = function(r) {
    if (r.details != undefined) {
        Array.each(r.details, function(row) {
            app.pageTracker.addRow(row);
        });
    }
};
app.pageTracker.addRow = function(row) {
    var c = new Element('div', {
        'class': 'trackItem',
        'id': row.id
    });
    new Element('div', { 'class': 'trackItemEdit' }).addEvent('click', app.pageTracker.editRow.bind()).inject(c);
    new Element('div', { 'class': 'trackItemDelete' }).addEvent('click', app.pageTracker.deleteRow.bind()).inject(c);
    new Element('div', { 'class': 'trackItemDescr', 'text':row.description }).inject(c);
    new Element('div', { 'class': 'trackItemHours', 'text':app.dt.humanTime(row.hours) }).inject(c);
    new Element('div', { 'class': 'trackItemSave trackItemBut', 'text':'save' }).inject(c);
    new Element('div', { 'class': 'trackItemUndo trackItemBut', 'text':'undo' }).inject(c);
    c.inject($('trackList'));
    return c;
};
app.pageTracker.editRow = function(eOrId) {
    var id = (typeof(eOrId) == 'object' ? eOrId.target.getParent().id : eOrId);
    app.pageTracker.itemToEditor($(id));
};
app.pageTracker.deleteRow = function(eOrId) {
    var id = (typeof(eOrId) == 'object' ? eOrId.target.getParent().id : eOrId);

    if (confirm("Delete this record, really????")) {
        app.pageTracker.del(id);
    }
};
app.pageTracker.itemToEditor = function(obj) {
    obj.setAttribute('editing', 'yes');
    var d = obj.getElements('div.trackItemDescr')[0];
    d.setAttribute('lastvalue', d.get('text'));
    var t = new Element('textarea', { 'class': 'trackItemDescr' });
    t.value = d.get('text');
    d.innerHTML = '';
    t.inject(d);

    var h = obj.getElements('div.trackItemHours')[0];
    h.setAttribute('lastvalue', h.get('text'));
    var i = new Element('input', { 'class': 'trackItemHours button' });
    i.value = h.get('text');
    h.innerHTML = '';
    i.inject(h);

    obj.getElements('div.trackItemUndo')[0].addEvent('click', function(e) {
        // On Undo
        var ref = e.target.getParent();
        var descr = ref.getElements('div.trackItemDescr')[0].getAttribute('lastvalue');
        var hours = ref.getElements('div.trackItemHours')[0].getAttribute('lastvalue');
        app.pageTracker.itemToNormal(ref, descr, hours);
    }).show();

    obj.getElements('div.trackItemSave')[0].addEvent('click', function(e) {
        // On Save
        var ref = e.target.getParent();
        var id = ref.id;
        var descr = ref.getElements('textarea.trackItemDescr')[0].value;
        var hours = ref.getElements('input.trackItemHours')[0].value;
        var hoursDec = app.dt.dbTime(hours);
        app.pageTracker.save(ref, id, hoursDec, descr);
    }).show();
};
app.pageTracker.itemToNormal = function(obj, descr, hours) {
    if (obj.id == '') { obj.destroy(); return; } // For undoing new
    obj.setAttribute('editing', 'no');
    var d = obj.getElements('div.trackItemDescr')[0];
    d.innerHTML = descr;
    d.setAttribute('lastvalue','');
    var h = obj.getElements('div.trackItemHours')[0];
    h.innerHTML = hours;
    h.setAttribute('lastvalue','');
    obj.getElements('div.trackItemUndo')[0].removeEvents().hide();
    obj.getElements('div.trackItemSave')[0].removeEvents().hide();
};

app.pageTracker.save = function(obj, id, hours, descr) {
    if (id == null || id == '') {
        rest.request('PUT', 'timetracking', 'addtime', {
            'user':app.impersonate.getUser(),
            'date': app.pageTracker.date,
            'hours': hours,
            'description': descr
        }, function (r) {
            obj.id = r.id;
            app.pageTracker.itemToNormal(obj, r.description, app.dt.humanTime(r.hours));
            app.pageTracker.reloadTotal();
        });

    } else {
        rest.request('PATCH', 'timetracking', 'edittime', {
            'user':app.impersonate.getUser(),
            'id': id,
            'hours': hours,
            'description': descr
        }, function (r) {
            app.pageTracker.itemToNormal($(id), r.description, app.dt.humanTime(r.hours));
            app.pageTracker.reloadTotal();
        });
    }
};
app.pageTracker.del = function(id) {
    rest.request('DELETE', 'timetracking', 'deletetime', { 'user':app.impersonate.getUser(), 'id':id}, function(r) {
        $(id).destroy();
        app.pageTracker.reloadTotal();
    });
};

app.pageTracker.addNew = function() {
    var newRec = app.pageTracker.addRow({
        'id':'',
        'hours': 0,
        'description': ''
    });
    app.pageTracker.itemToEditor(newRec);
};

app.pageTracker.hourShortage = function(date, hours) {
    hours = hours.toFloat();
    if (app.prefs == null || app.prefs == undefined || app.prefs.workhours == undefined) {
        return 'yes';
    }
    if (app.prefs.workdays.indexOf( app.dt.weekday(date) ) == -1) { return 'weekend'; }
    if (app.prefs.workhours <= hours) { return 'no'; }
    return 'yes';
};
