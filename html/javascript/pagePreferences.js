app.pagePreferences = {};
app.pagePreferences.init = function() {
    $('pagePreferences').onOpen = app.pagePreferences.onOpen.bind();

    app.button.onclick('prefsSave', app.pagePreferences.save.bind());
};
app.pagePreferences.onOpen = function() {
    app.pagePreferences.loadPrefs();
};
app.pagePreferences.loadPrefs = function() {
    rest.request('GET', 'preferences', 'getpreferences', { 'user': app.impersonate.getUser() }, function(p) {
				p.workdays = p.workdays.toString(); // PHP converts this to int if only 1 day selected
        app.prefs = p;
        app.pagePreferences.setPrefsOnScreen();
    });
};
app.pagePreferences.setPrefsOnScreen = function() {
    // Set hours
		var data = app.prefs;
    app.forms.setValue('prefsHours', app.dt.humanTime(data.workhours));

    // Switch all CBs off first
    $$('input.prefsDay').each(function(c) { c.checked = false; });
    var days = data.workdays.split(',');
    Array.each(days, function(day) {
        var fld = 'prefsCB' + day;
        var cb = $(fld);
        if (cb == null) { return; }
        cb.checked = true;
    });
};

app.pagePreferences.save = function() {
    var hours = $('prefsHours').value;
    var days = [];
    $$('input.prefsDay').each(function(c) {
        if (c.checked) {
            days.push( c.id.replace('prefsCB',''));
        }
     });
    days = days.toString();

    rest.request('PUT', 'preferences', 'savepreferences', { 'user':app.impersonate.getUser(), 'workhours':app.dt.dbTime(hours), 'workdays':days }, function(p) {
        app.prefs = p;
        app.page.show('pageTracker');
        app.message.show("Preferences saved");
    });
};
