// Class for easy REST api communication
// Works with simpleJWT PHP class
var rest = { };

// Configuration
rest.config = {
    path: '/',
    defaultError: function(msg) { alert(msg); },
    onUnauthorized: function() { }
};
rest.config.setRestPath = function(bp) { rest.config.path = bp; };
rest.config.setDefaultError = function(fn) { rest.config.defaultError = fn; };
rest.config.setOnUnauthorized = function(fn) { rest.config.onUnauthorized = fn; };

rest.logout = function() { rest.jwt.clearBearer(); };

// AJAX Request - don't use the auth variable. It's for JWT only
rest.request = function(method, category, request, data, funcSuccess, funcFailure, auth) {
    // Does AJAX requests to connect with REST API (backend)
    method = method.toUpperCase();
    var heads = { 'X-HTTP-Method-Override': method };
    rest.jwt.addAuthHeaders(heads, auth);

    var r = new Request({
        url: rest.config.path + '/' + category + '/' + request,
        method: (method == 'GET' ? 'GET' : 'POST'),
        headers: heads,

        onComplete: function() {
            if (this.status == 200) {
                // Success
                var data = rest.response.decode(this.response);
                window.r = this.response;
                if (funcSuccess != undefined) {
                    funcSuccess(data);
                }

            } else {
                // Failure
                var msg = 'Connection error'; // Default
                if (this.response != undefined && this.response.text != undefined) {
                    msg = this.response.text;
                }
                if (this.status == 401) { rest.config.onUnauthorized(); }
                if (funcFailure != undefined) {
                    funcFailure(msg);
                } else {
                    rest.config.defaultError(msg);
                }
            }
        }
    });
		if (method == 'GET' || method == 'DELETE') {
			r.get(data);
		} else {
			r.post(data);
		}
};

rest.response = {};
rest.response.decode = function(response) {
    var data = { 'error':'invalid json' };
    if (response.text != undefined) {
        try {
            data = JSON.decode(response.text);
        } catch (e) { }
    }
    return data;
};


// JWT
rest.jwt = {};
rest.jwt.t = null;
rest.jwt.tStart = null;
rest.jwt.cookieOptions = { domain: window.location.hostname, duration: 1 };
rest.jwt.authenticate = function(user, pass, onLogin, onError) {
    rest.request('POST', 'authentication', 'authenticate', {}, function(r) {
        rest.jwt.setBearer(r.jwt);
        if (onLogin != undefined) { onLogin(); }

    }, function(r) {
        rest.jwt.clearBearer();
        if (onError != undefined) {
            onError(r);
        } else {
            rest.config.defaultError(r);
        }
    }, {'user':user, 'password':pass});
};
rest.jwt.getBearer = function() {
    if (rest.jwt.t != null) { return rest.jwt.t; }
    return Cookie.read('jwt'); // Return null if non-existing
};
rest.jwt.setBearer = function(token) {
    rest.jwt.t = token;
    rest.jwt.tStart = Date.now();
    Cookie.write('jwt', token, rest.jwt.cookieOptions);
    Cookie.write('jwtStart', rest.jwt.tStart, rest.jwt.cookieOptions);
};
rest.jwt.clearBearer = function() {
    rest.jwt.t = null;
    Cookie.dispose('jwt', rest.jwt.cookieOptions);
    Cookie.dispose('jwtStart', rest.jwt.cookieOptions);
};
rest.jwt.addAuthHeaders = function(headers, auth) {
    if (auth != undefined) {
        // Add HTTP Basic authentication headers
        headers['Authorization'] = 'Basic ' + (auth.user + ":" + auth.password).toBase64();

    } else {
        // Try to add JWT Bearer
        var bearer = rest.jwt.getBearer();
        if (bearer) { headers['Authorization'] = 'Bearer ' + bearer }

        rest.jwt.checkTokenRenew();
    }
};
rest.jwt.checkTokenRenew = function() {
    // Check if token needs a refresh
    var tokenStart = (rest.jwt.tStart ? rest.jwt.tStart : Cookie.read('jwtStart'));
    if (tokenStart == null) { return; } // No token to refresh
    var tokenAge = (Date.now() - tokenStart) / 1000; // In seconds
    if (tokenAge > 60 * 5) { // Renew token every 5 minutes
        rest.jwt.tokenRenew();
    }
};
rest.jwt.tokenRenew = function() {
    rest.jwt.tStart = Date.now(); // Prevent looping
    rest.request('POST', 'authentication', 'renew', {}, function(r) {
        rest.jwt.setBearer(r.jwt);
    }, function(r) {
        // Issue while renewing - don't show
    });
};


// Adding base64 functions as builtin base64 does NOT handle multibyte chars right (minified)
/*
---
script: Base64.js
description: String methods for encoding and decoding Base64 data
license: MIT-style license.
authors: Ryan Florence (http://ryanflorence.com), webtoolkit.info
requires:
 - core:1.2.4: [String]
provides: [String.toBase64, String.decodeBase64]
*/
!function(){var r={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(t){var e,o,n,c,h,a,d,i="",C=0;for(t=r._utf8_encode(t);C<t.length;)e=t.charCodeAt(C++),o=t.charCodeAt(C++),n=t.charCodeAt(C++),c=e>>2,h=(3&e)<<4|o>>4,a=(15&o)<<2|n>>6,d=63&n,isNaN(o)?a=d=64:isNaN(n)&&(d=64),i=i+this._keyStr.charAt(c)+this._keyStr.charAt(h)+this._keyStr.charAt(a)+this._keyStr.charAt(d);return i},decode:function(t){var e,o,n,c,h,a,d,i="",C=0;for(t=t.replace(/[^A-Za-z0-9\+\/\=]/g,"");C<t.length;)c=this._keyStr.indexOf(t.charAt(C++)),h=this._keyStr.indexOf(t.charAt(C++)),a=this._keyStr.indexOf(t.charAt(C++)),d=this._keyStr.indexOf(t.charAt(C++)),e=c<<2|h>>4,o=(15&h)<<4|a>>2,n=(3&a)<<6|d,i+=String.fromCharCode(e),64!=a&&(i+=String.fromCharCode(o)),64!=d&&(i+=String.fromCharCode(n));return i=r._utf8_decode(i)},_utf8_encode:function(r){r=r.replace(/\r\n/g,"\n");for(var t="",e=0;e<r.length;e++){var o=r.charCodeAt(e);128>o?t+=String.fromCharCode(o):o>127&&2048>o?(t+=String.fromCharCode(o>>6|192),t+=String.fromCharCode(63&o|128)):(t+=String.fromCharCode(o>>12|224),t+=String.fromCharCode(o>>6&63|128),t+=String.fromCharCode(63&o|128))}return t},_utf8_decode:function(r){for(var t="",e=0,o=c1=c2=0;e<r.length;)o=r.charCodeAt(e),128>o?(t+=String.fromCharCode(o),e++):o>191&&224>o?(c2=r.charCodeAt(e+1),t+=String.fromCharCode((31&o)<<6|63&c2),e+=2):(c2=r.charCodeAt(e+1),c3=r.charCodeAt(e+2),t+=String.fromCharCode((15&o)<<12|(63&c2)<<6|63&c3),e+=3);return t}};String.implement({toBase64:function(){return r.encode(this)},decodeBase64:function(){return r.decode(this)}})}();
