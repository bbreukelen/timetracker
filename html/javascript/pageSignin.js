app.pageSignin = {
    recoverEmail: null
};

app.pageSignin.init = function() {
    $('pageSignin').onOpen = app.pageSignin.onOpen.bind();
    // Init register link
    $('signInRegisterLink').addEvent('click', app.pageSignin.appRegisterLinkClick.bind());
    $('signInSignInLink').addEvent('click', app.pageSignin.appSigninLinkClick.bind());
    $('signInRecoverLink').addEvent('click', app.pageSignin.appRecoverLinkClick.bind());
    app.button.onclick('signInSignIn', app.pageSignin.signin.bind());
    app.button.onclick('signInRegister', app.pageSignin.register.bind());
    app.button.onclick('signInRecover', app.pageSignin.recover.bind());
    app.button.onclick('signInChange', app.pageSignin.change.bind());
};
app.pageSignin.onOpen = function() {
    app.pageSignin.appSigninLinkClick();
};

app.pageSignin.appRegisterLinkClick = function() {
    $('signInTitle').set('text','Register');
    $('signInSignIn').hide();
    $('signInRegister').show();
    $('signInRecover').hide();
    $('signInChange').hide();
    $('signInSignInLink').show();
    $('signInRegisterLink').hide();
    $('signInRecoverLink').hide();
    $('signInUserFieldC').show();
    $('signInPasswordFieldC').fade(1);
    $('signInUserField').placeholder = 'e-mail addresss';
    $('signInPasswordField').placeholder = 'password';
    $('signInUserFieldC').setAttribute('recover','no');
};

app.pageSignin.appSigninLinkClick = function() {
    $('signInTitle').set('text','Sign In');
    $('signInSignIn').show();
    $('signInRegister').hide();
    $('signInRecover').hide();
    $('signInChange').hide();
    $('signInSignInLink').hide();
    $('signInRegisterLink').show();
    $('signInRecoverLink').show();
    $('signInUserFieldC').show();
    $('signInPasswordFieldC').fade(1);
    $('signInUserField').placeholder = 'e-mail addresss';
    $('signInPasswordField').placeholder = 'password';
    $('signInUserFieldC').setAttribute('recover','no');
};
app.pageSignin.appRecoverLinkClick = function() {
    $('signInTitle').set('text','Recover Password');
    $('signInSignIn').hide();
    $('signInRegister').hide();
    $('signInRecover').show();
    $('signInChange').hide();
    $('signInSignInLink').show();
    $('signInRegisterLink').hide();
    $('signInRecoverLink').hide();
    $('signInUserFieldC').show();
    $('signInPasswordFieldC').fade(0);
    $('signInUserField').placeholder = 'e-mail addresss';
    $('signInPasswordField').placeholder = 'password';
    $('signInUserFieldC').setAttribute('recover','no');
};
app.pageSignin.showRecoverFields = function() {
    $('signInTitle').set('text','Recover Password');
    $('signInSignIn').hide();
    $('signInRegister').hide();
    $('signInRecover').show();
    $('signInChange').hide();
    $('signInSignInLink').show();
    $('signInRegisterLink').hide();
    $('signInRecoverLink').hide();
    $('signInUserFieldC').show();
    $('signInPasswordFieldC').fade(1);
    $('signInUserField').placeholder = 'validation code';
    $('signInPasswordField').placeholder = 'new password';
    $('signInUserFieldC').setAttribute('recover','yes');
};
app.pageSignin.showChangePasswordFields = function() {
    $('signInTitle').set('text','Change Password');
    $('signInSignIn').hide();
    $('signInRegister').hide();
    $('signInRecover').hide();
    $('signInChange').show();
    $('signInSignInLink').hide();
    $('signInRegisterLink').hide();
    $('signInRecoverLink').hide();
    $('signInUserFieldC').hide();
    $('signInPasswordFieldC').fade(1);
    $('signInPasswordField').placeholder = 'new password';
};

app.pageSignin.signin = function() {
    app.message.close();
    var email = $('signInUserField').value;
    if (app.forms.emailValid(email) === false) { app.message.show("Incorrect email address"); return; }
    app.button.working('signInSignIn');
    var pass = $('signInPasswordField').value;

    rest.jwt.authenticate(email, pass, function() {
        app.button.done('signInSignIn');
        app.forms.setValue($('signInUserField'), '');
        app.forms.setValue($('signInPasswordField'), '');
        app.message.close();
        app.getSessionInfo(function() { app.showStartPage(); });
    }, function(r) {
        app.button.done('signInSignIn');
        app.message.show(r);
    });
};

app.pageSignin.register = function() {
    app.message.close();
    var email = $('signInUserField').value;
    if (app.forms.emailValid(email) === false) { app.message.show("Incorrect email address"); return; }
    var pass  = $('signInPasswordField').value;
    if (pass.length < 6) { app.message.show("Your password is too short"); return; }
    app.button.working('signInRegister');

    var data = {'email':email, 'password':pass};
    rest.request('POST', 'usermanagement', 'register', data, function(r) {
        app.button.done('signInRegister');
        app.forms.setValue($('signInUserField'), '');
        app.forms.setValue($('signInPasswordField'), '');
        app.pageSignin.appSigninLinkClick();
        app.message.show("Thank you for registering. Please check your email to validate your account.");
    }, function(err) {
        app.button.done('signInRegister');
        app.message.show(err);
    });
};

app.pageSignin.recover = function() {
    app.message.close();

    if ($('signInUserFieldC').getAttribute('recover') != 'yes') {
        // RECOVER STEP 1
        var email = $('signInUserField').value;
        if (app.forms.emailValid(email) === false) {
            app.message.show("Incorrect email address");
            return;
        }

        app.button.working('signInRecover');
        rest.request('POST', 'usermanagement', 'passwordresetrequest', {'email': email}, function (r) {
            app.pageSignin.recoverEmail = email;
            app.button.done('signInRecover');
            app.forms.setValue($('signInUserField'), '');
            app.forms.setValue($('signInPasswordField'), '');
            app.message.show("Please check your email. You will be invited to reset your password.");
            app.pageSignin.showRecoverFields();
        }, function (err) {
            app.button.done('signInRecover');
            app.message.show(err);
        });

    } else {
        // RECOVER STEP 2
        var email = app.pageSignin.recoverEmail;
        var code = $('signInUserField').value;
        if (code.length < 1) { app.message.show("Incorrect validation code"); return; }
        var pass  = $('signInPasswordField').value;
        if (pass.length < 6) { app.message.show("Your password is too short"); return; }

        app.button.working('signInRecover');
        rest.request('POST', 'usermanagement', 'passwordreset', {'email': email, 'validationcode':code, 'password':pass}, function (r) {
            app.button.done('signInRecover');
            app.forms.setValue($('signInUserField'), email);
            app.forms.setValue($('signInPasswordField'), '');
            app.message.show("You can now sign in with your new password");
            app.pageSignin.appSigninLinkClick();
        }, function (err) {
            app.button.done('signInRecover');
            app.message.show(err);
        });
    }
};

app.pageSignin.change = function() {
    app.message.close();
    var pass  = $('signInPasswordField').value;
    if (pass.length < 6) { app.message.show("Your new password is too short"); return; }
    app.button.working('signInChange');

    rest.request('PATCH', 'usermanagement', 'changepassword', {'id':app.impersonate.getUser(), password:pass}, function(r) {
        app.button.done('signInChange');
        app.forms.setValue($('signInPasswordField'), '');
        app.page.show('pageTracker');
        app.pageSignin.appSigninLinkClick();
        app.message.show("Your password has been changed");
    }, function(err) {
        app.button.done('signInChange');
        app.message.show(err);
    });
};
