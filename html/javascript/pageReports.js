app.pageReports = {
    dateStart: app.dt.lastMonth(),
    dateEnd: app.dt.today(),
    totalHours: 0
};
app.pageReports.init = function() {
    $('pageReports').onOpen = app.pageReports.onOpen.bind();

    $('reportsFilterCalStart').makeCalendar({
        'position': 'center', 'caption_color': '#777777', 'week_color': '#777777', 'format': '%Y-%m-%d', value: app.pageTracker.date,
        'onSelectDate': function (dt) {
            app.pageReports.dateStart = dt;
            app.pageReports.onOpen();
        }
    });
    $('reportsFilterCalEnd').makeCalendar({
        'position': 'center', 'caption_color': '#777777', 'week_color': '#777777', 'format': '%Y-%m-%d', value: app.pageTracker.date,
        'onSelectDate': function (dt) {
            app.pageReports.dateEnd = dt;
            app.pageReports.onOpen();
        }
    });
    $('reportsExport').addEvent('click', app.pageReports.doExport.bind());
};
app.pageReports.onOpen = function() {
    if (app.prefs == null) {
        app.pageTracker.getPrefs(app.pageReports.load.bind());
    } else {
        app.pageReports.load();
    }
};

app.pageReports.clean = function() {
    $('reportsTotalHours').set('text', '');
    $('reportsList').innerHTML = '';
    app.pageReports.totalHours = 0;
};
app.pageReports.load = function() {
    app.pageReports.clean();
    app.pageReports.showDates();
    $('reportsList').setAttribute('working', 'yes');
    rest.request('GET', 'timereporting', 'listdays', { user: app.impersonate.getUser(), date_start: app.pageReports.dateStart, date_end: app.pageReports.dateEnd }, function(r) {
        $('reportsList').setAttribute('working', 'no');
        app.pageReports.showList(r);
        app.pageReports.showTotal();
    }, function(err) {
        $('reportsList').setAttribute('working', 'no');
        app.message.show(err);
    });
};
app.pageReports.showDates = function() {
    $('reportsFilterDateStart').set('text', app.dt.humanDate(app.pageReports.dateStart, app.dt.TYPE_DATE_MID));
    $('reportsFilterDateEnd').set('text', app.dt.humanDate(app.pageReports.dateEnd, app.dt.TYPE_DATE_MID));
};
app.pageReports.showTotal = function() {
    $('reportsTotalHours').set('text', app.dt.humanTime(app.pageReports.totalHours));
};
app.pageReports.showList = function(resp) {
    // Sort dataset
    var data = {};
    if (resp != undefined && typeof(resp) == 'object') {
        Array.each(resp, function(item) {
            data[item.date] = item;
        })
    }

    // Loops through entire selected period and shows data if exists
    var dates = app.pageReports.makeDates();
    Array.each(dates, function(date) {
        app.pageReports.addRow(date, data[date]);
    });

    if (dates.length == 0) {
        new Element('div', { 'class':'reportsNoResults', 'text':'Please adjust the date range' }).inject($('reportsList'));
    }
};
app.pageReports.makeDates = function() {
    var dates = [];
    var date = app.pageReports.dateStart;
    while (date <= app.pageReports.dateEnd) {
        dates.push(date);
        date = app.dt.addDay(date, 1);
    }
    Array.reverse(dates);
    return dates;
};
app.pageReports.addRow = function(date, row) {
    if (row == undefined) { row = { hours: 0 }; }
    var hs = app.pageTracker.hourShortage(date, row.hours);
    var c = new Element('div', {  'class': 'reportsItem', hourshortage:hs, 'date':date  });
    new Element('div', { 'class': 'reportsIDate', 'html': app.dt.humanDate(date, app.dt.TYPE_DATE_MID) }).inject(c);
    new Element('div', { 'class': 'reportsIHours','text': app.dt.humanTime(row.hours) }).inject(c);
    c.addEvent('click', app.pageReports.rowClicked.bind(this, date));
    c.inject($('reportsList'));
    app.pageReports.totalHours += row.hours;
    return c;
};
app.pageReports.rowClicked = function(date) {
    app.pageTracker.date = date;
    app.page.show('pageTracker');
};
app.pageReports.doExport = function() {
    rest.request('GET', 'timereporting', 'exportdays', {
        user: app.impersonate.getUser(),
        date_start: app.pageReports.dateStart,
        date_end: app.pageReports.dateEnd,
        format: 'link'}, function(d) {
            if ($('exportIframe') != null) { $('exportIframe').destroy(); }
            new Element('iframe', { 'id':'exportIframe', src: d.link }).hide().inject(document.body);
        }
    );

};
