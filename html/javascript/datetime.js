app.dt = {
    TYPE_DATE_LONG: '%A %B %e%o, %Y',
    TYPE_DATE_MID: '%b %e%o %Y',
    TYPE_DATE_SHORT: '%m-%d-%Y'
};
app.dt.make = function(date) {
	if (date == undefined) {
		var d = new Date();
	} else {
		var d = new Date(date);
	}
	var tzo = d.get('timezoneoffset');
	d.increment('minute', tzo);
	return d;
};
app.dt.today = function() {
    return app.dt.make().format('%Y-%m-%d');
};
app.dt.lastMonth = function() {
    return app.dt.make().increment('month', -1).format('%Y-%m-%d')
};
app.dt.addDay = function(start, add) {
    return app.dt.make(start).increment('day', add).format('%Y-%m-%d');
};
app.dt.humanDate = function(dt, type) {
    if (dt == null || dt == 0) { return '-'; }
    var out = app.dt.make(dt).format(type);
    if (out == 'invalid date') { out = '-'; }
    return out;
};
app.dt.humanTime = function (time) {
    time = time * 60; // # of Minutes
    var hours = Math.floor(time / 60);
    var mins = Math.round(time - (hours * 60));

    return hours.toString().pad(2, '0', 'left').toString() + ':' + mins.toString().pad(2, '0', 'left').toString();
};
app.dt.fixTime = function(time) {
    // To make sure human entered time is flexible and accepts 09.00, 0900, 1300 and 09
    var hours = 0;
    var mins = 0;
    time = time.replace('.',':').replace(',',':').replace(';',':');
    if (time.indexOf(':') > -1) {
        var split = time.split(':');
        var hours = String.toInt(split[0].replace(/\D/, ''));
        var mins  = String.toInt(split[1].replace(/\D/, ''));

    } else {
        time = time.replace(/\D/, '');
        var t = String.toInt(time);
        var hours = 0;
        var mins = t;
    }

    return app.dt.humanTime(hours + (mins * (1/60)));
};
app.dt.dbTime = function(time) {
    time = app.dt.fixTime(time);
    var split = time.split(':');
    return String.toInt(split[0]) + (String.toInt(split[1]) * (1/60));
};
app.dt.weekday = function(date) {
    // Returns weekday in mysql format Sunday = 0 (outputs a string)
    return app.dt.make(date).format('%w');
};
