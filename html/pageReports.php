<div class="mainTitle">Reports</div>
<div id="reportsHeader">
    <div id="reportsTotalText">Total time</div>
    <div id="reportsTotalHours"></div>
</div>
<div id="reportsFilters">
    <div class="reportsFilter">
        <div class="reportsFilterTxt rfi">Start date</div>
        <div class="reportsFilterDate rfi" id="reportsFilterDateStart"></div>
        <div class="reportsFilterCal rfi" id="reportsFilterCalStart"></div>
    </div>
    <div class="reportsFilter">
        <div class="reportsFilterTxt rfi">End date</div>
        <div class="reportsFilterDate rfi" id="reportsFilterDateEnd"></div>
        <div class="reportsFilterCal rfi" id="reportsFilterCalEnd"></div>
    </div>
    <div id="reportsExport" class="button">export</div>
</div>

<div id="reportsList"></div>