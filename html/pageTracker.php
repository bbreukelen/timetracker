<div class="mainTitle">Track Time</div>
<div id="trackDateHeader">
    <div id="trackYesterday"></div>
    <div id="trackDate" class="noSelect"><span id="trackDateText"></span></div>
    <div id="trackTomorrow"></div>
</div>

<div id="trackTotals">
    <div id="trackTotalText">Day Total</div>
    <div id="trackTotalHours"></div>
</div>

<div id="trackList"></div>

<div id="trackAddNew">+</div>
