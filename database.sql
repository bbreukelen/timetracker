# ************************************************************
# Sequel Pro SQL dump
# Version 4568
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: timetracker.yourdomain.com (MySQL 5.5.40-0+wheezy1)
# Database: timetracker
# Generation Time: 2016-02-21 23:06:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table hours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hours`;

CREATE TABLE `hours` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `user_id` varchar(36) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `hours` decimal(8,6) DEFAULT NULL,
  `description` text,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table preferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `preferences`;

CREATE TABLE `preferences` (
  `user_id` varchar(36) NOT NULL DEFAULT '',
  `workdays` varchar(25) DEFAULT '1,2,3,4,5',
  `workhours` decimal(4,2) DEFAULT '8.00',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `role` varchar(15) NOT NULL DEFAULT 'user',
  `password` varchar(255) NOT NULL DEFAULT '',
  `email_validated` tinyint(1) DEFAULT '0',
  `validation_code` varchar(36) DEFAULT NULL,
  `reset_status` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `lastlogin_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

