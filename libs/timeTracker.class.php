<?php

class timeTracker {

    public function __construct() {
        session_start();
        date_default_timezone_set('Europe/Amsterdam');
    }

    public function sendEmail($to, $subject, $body, $attachment='') {
        global $config;
        // Attachment is '' or Array('filename' => 'filename.xxx', 'content' => 'binary stuff')
        require_once($config['libspath'].'/pjmail/pjmail.class.php');
        $mail = new PJmail();
        $mail->setAllFrom($config['mail']['fromMail'], $config['mail']['fromName']);
        foreach ( explode(";", $to) as $toAddr) {
            $mail->addrecipient($toAddr);
        }
        // $mail->addbcc($config['mail']['fromMail']); // For testing purposes ONLY - never to be enabled in production
        $mail->addsubject($subject);
        $mail->text = $body;

        if ($attachment != '') {
            $mail->addbinattachement($attachment['filename'], $attachment['content']);
        }
        return $mail->sendmail();
    }

    public function getMailTemplate($template, $data) {
        global $config;
        $template = $config['templates'][$template];
        if (! $template) { error_log("Template {$template} not found"); return ''; }
        if (! file_exists($template)) { error_log("Template {$template} not found");  return ''; }
        $content = file_get_contents($template);
        foreach ($data as $field=>$value) {
            $content = preg_replace("'\[$field\]'", $value, $content);
        }
        return $content;
    }



}