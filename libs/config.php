<?PHP

// Database connection details
$config['dbhost'] = 'localhost';
$config['dbuser'] = 'timetracker';
$config['dbpwd']  = 'timetracker';
$config['dbname'] = 'timetracker';
$config['dbtz']   = 'Europe/Amsterdam';

// Paths
$config['domain'] = 'timetracker.whatever.com';
$config['basepath'] = '/var/www/whatever/subdomains/timetracker';
$config['webpath']  = 'http://timetracker.whatever.com';
$config['libspath'] = $config['basepath'] . '/libs';
$config['emailtemplatesfolder'] = $config['libspath'] . '/emailTemplates';

// BCrypt password processor strength (test - must not take over 0.5 seconds to create) - check debug log for time it takes
$config['jwt']['key'] = '-add-a-key-here-using-the-function-on-the-right-here-'; // Make with base64_encode(openssl_random_pseudo_bytes(64))
$config['jwt']['algorithm'] = 'HS512';
$config['jwt']['sessiontime'] = 15; // Minutes
$config['jwt']['servername'] = $config['webpath'];
$config['password_cost'] = 13;

// Email templates etc
$config['mail']['fromMail'] = "timetracker@whatever.com";
$config['mail']['fromName'] = "Time Tracker";

// Templates
$config['templates']['registerThanks'] = $config['emailtemplatesfolder'] . '/registerThanks';
$config['templates']['passwordReset'] = $config['emailtemplatesfolder'] . '/passwordReset';
$config['templates']['htmlExport'] = $config['emailtemplatesfolder'] . '/htmlExport';
